const gulp = require('gulp');
const jade = require('gulp-jade');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const stylus = require('gulp-stylus');
const cleanCSS = require('gulp-clean-css');
const rigger = require('gulp-rigger');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const path = 'app'; //'web/mobile_assets';


/*
 *   CSS
 */
gulp.task('css', () => {
    gulp.src('./assets/css/app.styl')
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['last 3 versions', '>1%'],
            cascade: false
        }))
        .pipe(gulp.dest('./' + path + '/css/'))
        .pipe(cleanCSS())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest('./' + path + '/css/'));
});



/*
 *   Script
 */
gulp.task('script', () => {
    gulp.src([
            'node_modules/babel-polyfill/dist/polyfill.min.js',
            './assets/js/components/Droplist.js',
            './assets/js/components/Checkbox.js',
            './assets/js/components/Submit.js',
            './assets/js/components/Tabs.js',
            './assets/js/components/Overlay.js',
            './assets/js/components/Range.js',
            './assets/js/components/Assay.js',
            './assets/js/components/Gmaps.js',
            './assets/js/app.js'
        ])
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./' + path + '/js/'))
        .pipe(uglify())
        .pipe(rename('app.min.js'))
        .pipe(gulp.dest('./' + path + '/js/'));

    // JQuery
    gulp.src('./assets/js/components/jquery.min.js').pipe(gulp.dest('./' + path + '/js/'));
    // JQuery

    // Masked Input
    gulp.src('./assets/js/components/masked.input.min.js').pipe(gulp.dest('./' + path + '/js/'));
    // Masked Input
});



/*
 *   Jade
 */
gulp.task('jade', () => {
    gulp.src(['./assets/template/*.jade'])
        .pipe(jade({
            pretty: true
        }))
        .pipe(rename(function (path) {
            path.extname = ".php"
        }))
        .pipe(gulp.dest('./' + path + '/'));
});



/*
 *   Img
 */
gulp.task('img', () => {
    gulp.src('./assets/img/**/*.*')
        .pipe(gulp.dest('./' + path + '/img/'))
});



/*
 *   SVG
 */
gulp.task('svg', () => {
    gulp.src('./assets/img/svg/*.*')
        .pipe(gulp.dest('./' + path + '/img/svg/'))
});



/*
 *   Images
 */
gulp.task('images', () => {
    gulp.src(['./assets/images/*.*', './assets/images/**/*.*'])
        .pipe(gulp.dest('./' + path + '/images/'))
});



/*
 *   Fonts
 */
gulp.task('fonts', () => {
    gulp.src('./assets/fonts/**/*.*')
        .pipe(gulp.dest('./' + path + '/fonts/'))
});



/*
 *   Libs
 */
gulp.task('libs', () => {
    gulp.src('./assets/libs/**/*.*')
        .pipe(gulp.dest('./' + path + '/libs/'))
});



/*
 *   Root
 */
gulp.task('root', () => {
    gulp.src(['./assets/root/**/*.*', './assets/root/.htaccess'])
        .pipe(gulp.dest('./app/'))
});



/*
*   Watcher
 */
gulp.task('stream', () => {
    watch('./assets/css/**/*.styl', () => {
        gulp.start('css');
    });

    watch(['./assets/js/*.js', './assets/js/**/*.js'], () => {
        gulp.start('script');
    });

    watch('./assets/img/**/*.*', () => {
        gulp.start('img');
    });

    watch('./assets/img/svg/*.*', () => {
        gulp.start('svg');
    });

    watch(['./assets/images/*.*', './assets/images/**/*.*'], () => {
        gulp.start('images');
    });

    watch(['./assets/template/**/*'], () => {
        gulp.start('jade');
    });

    watch('./assets/fonts/**/*.*', () => {
        gulp.start('fonts');
    });

    watch('./assets/libs/**/*.*', () => {
        gulp.start('libs');
    });

    watch(['./assets/root/**/*.*', './assets/root/.htaccess'], () => {
        gulp.start('root');
    });
});



/*
 *   Default
 */
gulp.task('default', ['script', 'css','img', 'svg', 'images', 'fonts', 'jade', 'root', 'libs', 'stream']);