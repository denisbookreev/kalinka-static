'use strict';

window.onload = () => {
    // init
    droplist.init();
    checkbox.init();

    // block empty links
    $('[href=#]').on('click', e => {
        e.preventDefault();
    });

    // sitemap
    $('.sitemap__item.disabled').on('click', e => {
        e.preventDefault();
    });

    $('.form .checkbox label').on('click', function () {
        $(this).parent().children('input').click();
    });

    $('.content__title.open-list').on('click', function () {
        const it = $(this);
        const menu = $('.content__menu');

        if ( it.hasClass('opened') ) {
            it.removeClass('opened');
            menu.fadeOut(200);
        } else {
            it.addClass('opened');
            menu.fadeIn(200);
        }
    });



    /**
     *  Footer Map
     */

    $('.open-footer-map').on('click', function () {
        let link = $(this);
        let map = $(`.footer-map[data-map=${link.attr('data-map')}]`);

        map.addClass('opened');
    });
    $('.footer-map__close').on('click', function  () {
        $(this).closest('.footer-map').removeClass('opened');
    });



    /**
     *  SubCut
     */

    $('.el-subcut__link').on('click', function () {
        let link = $(this);
        let linkTitle = link.children('.el-subcut__link-text');
        let content = link.parent().find('.el-subcut__content');

        if ( !link.hasClass('opened') ) {
            link.addClass('opened');
            linkTitle.slideUp(500);
            content.slideDown(500);
        } else {
            link.removeClass('opened');
            linkTitle.slideDown(500);
            content.slideUp(500);
        }
    });



    /**
     *  Carousel
     */

    $(".owl-carousel").owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        animateOut: 'fadeOut'
    });



    /**
     *  To Up
     */

    $('#upper').on('click', function () {
        $('body').animate({
            scrollTop: 0
        }, 700);
    });



    /**
     *  More Desc
     */

    $('.more-desc').on('click', function () {
        let link = $(this);

        if ( !link.hasClass('active') ) {
            $('.more-desc__full').show();
            $('.more-desc__trunc').hide();
            link.addClass('active');
            link.text('Скрыть');
        } else {
            $('.more-desc__full').hide();
            $('.more-desc__trunc').show();
            link.removeClass('active');
            link.text('Полное описание');
        }
    });



    /**
     *  Main Menu
     */

    $('#open-menu').on('click', function () {
        $('.header__menu').slideToggle(400);
    });
    $('#close-menu').on('click', function () {
        $('.header__menu').slideUp(200);
    });

    $('.header__menu-item.has-list').on('click', function () {
        const list = $(this).children('.header__menu-sublist');

        if ( $(this).hasClass('opened') ) {
            $(this).removeClass('opened');
            list.slideUp(300);
        } else {
            $('.header__menu-item.has-list').removeClass('opened');
            $('.header__menu-sublist').slideUp(300);

            $(this).addClass('opened');
            list.slideDown(300);
        }
    });



    /**
     *  Price Chooser
     */

    $('.estate-preview__currency-btns a').on('click', function () {
        let link = $(this);
        let parent = link.closest('.estate-preview__cost');
        let links = parent.find('.estate-preview__currency-btns a');
        let costs = parent.find('span');

        if ( !link.hasClass('active') ) {
            let data = link.attr('data-currency');
            costs.removeClass('active');
            parent.find(`span[data-id=${data}]`).addClass('active');

            links.removeClass('active');
            link.addClass('active');

            $('.estate-preview__currency-btns a').click();
        }
    });



    /**
     *  More About
     */

    $('.more-about').on('click', function () {
        let link = $(this);
        let linkAlt = link.attr('data-alt');
        let linkText = link.text();
        let about = link.parent().find('.hide-about');

        if ( !about.hasClass('hide-about_opened') ) {
            link.text(linkAlt);
            link.attr('data-alt', linkText);
            link.attr('data-text', linkText);

            about.addClass('hide-about_opened');
        } else {
            link.text(link.attr('data-alt'));
            link.attr('data-alt', linkText);
            link.attr('data-text', linkText);

            about.removeClass('hide-about_opened');
        }
    });



    /**
     *  Select Trigger
     */

    $('.select-trigger').on('click', function () {
        let link = $(this);
        let dataTrigger = link.attr('data-select-trigger');
        let droplists = $('.el-droplist.select-triggered');
        let droplistTriggered = $(`.el-droplist.select-triggered[data-select-triggered=${dataTrigger}]`);
        let selects =  droplists.find('select');
        let selectTriggered =  droplistTriggered.find('select');

        droplists.hide();
        droplist.hide(droplists);
        droplistTriggered.show();
        selects.attr('disabled', 'disabled');
        selectTriggered.attr('disabled', false);
    });
    if (window.location.pathname == '/gorod/' || window.location.pathname == '/zagorod/' || window.location.pathname == '/commercial/' ) {
      $('.el-droplist__li:first-child').click();
    }
    
    /**
     *  Masked Inputs
     */

    if ($('*').is('input[type=tel]')) {
        $('input[type=tel]').mask('+7 (999) 999-99-99');
    }



    /**
     *  History
     */

    $('.history__event').on('click', function () {
        let event = $(this);
        let offsetTop = event.offset().top;
        let allEvents = $('.history__event');

        allEvents.removeClass('full-point');
        event.addClass('full-point');
        if ( $(window).width() < 768 ) {
            $('body').animate({
                scrollTop: offsetTop - 50
            }, 500);
        } else {
            $('body').animate({
                scrollTop: offsetTop - 120
            }, 500);
        }
    });


    /**
     * Search page
     */

    if ( $('*').is('#search-result__items') ) {
        submit.form = $('form[data-form=search-page]');
        let form = submit.form.attr('data-form');
        let action = submit.form.attr('action');
        let data = submit.form.serialize();

        submit.submit(form, action, data);
    }
    /**
     *  Map regions checkbox
     */
     $('.el-map-check > label').on('click', function () {
       let parent = $(this).parent();

       if(parent.hasClass('active')) {
         parent.removeClass('active');
       } else {
         parent.addClass('active');
       }
     })
};
