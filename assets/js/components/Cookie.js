class Cookie {
    constructor() {
        let it = this;
        //
    }

    // возвращает cookie с именем name, если есть, если нет, то undefined
    get(name) {
        let it = this;

        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }


    // устанавливает cookie с именем name и значением value
    // options - объект с свойствами cookie (expires, path, domain, secure)
    set(name, value, options) {
        let it = this;

        options = options || {};

        let expires = options.expires;

        if (typeof expires == "number" && expires) {
            let d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        let updatedCookie = name + "=" + value;

        for (let propName in options) {
            updatedCookie += "; " + propName;
            let propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }

    // удаляет cookie с именем name
    remove(name) {
        let it = this;

        it.set(name, "", {
            expires: -1
        })
    }
}
const cookie = new Cookie();