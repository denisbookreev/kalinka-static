class Router {
    constructor() {
        let it = this;

        it.path = window.location.pathname;

        it.page = it.getPage(it.path);
        it.hash = it.getHash();

        console.info( 'Page: ' + it.page );
    }


    /**
     *
     * Return page name (without .html or .php)
     * @param path
     * @returns {*}
     */
    getPage(path) {
        if ( path.indexOf('.html') !== -1 ) {
            path = path.substring(1, path.indexOf('.html'));

        } else if ( path.indexOf('.php') !== -1 ) {
            path = path.substring(1, path.indexOf('.php'));

        } else {
            path = path.slice(1);

            if ( path === '' ) {
                path = 'index';
            }
        }

        return path;
    }


    /**
     *
     * Set page and redirect to
     * @param path
     * @param newTab
     * @returns {*}
     */
    setPage(path, newTab) {


        return path;
    }


    /**
     * Return hash without # or null
     * @returns {*}
     */
    getHash() {
        let it = this;
        let hash = window.location.hash;

        if ( hash !== '#' && hash !== '' ) {
            hash = hash.slice(1);
        } else {
            hash = null;
        }

        return hash;
    }


    /**
     *
     * @param hash
     */
    setHash(hash) {
        window.location.hash = hash;

        console.log(`Hash ${hash} is set`);
    }


    /**
     * Get GET-parameters
     * @param param
     * @returns {*}
     */
    getGet(param) {
        let path = window.location.search;
        let GET = {};
        let _GET = path.substring(1).split("&");

        for(let i = 0; i < _GET.length; i++) {
            let getVar = _GET[i].split("=");
            GET[getVar[0]] = typeof(getVar[1]) == "undefined" ? "" : getVar[1];
        }

        if (GET[param]) {
            return GET[param];
        } else {
            return null;
        }
    }
}
const router = new Router();