class Assay {
    constructor () {
        let it = this;

        it.pattern = {
            email: /[\S]+@[\S]+\.[\S]+/i
        };
    }


    /**
     * E-mail assay
     * @param email
     */
    email (email) {
        let it = this;

        return it.pattern.email.test(email);
    }
}
const assay = new Assay();