class Overlay {
    constructor () {
        let it = this;

        it.overlay = $('#overlay');
        it.popupLinks = $('.open-popup');

        it.popupLinks.on('click', function () {
            let popup = $(this).attr('data-popup');

            it.open(popup);
        });

        it.closePopupLink = $('.popup__close');
        it.closePopupLink.on('click', function () {
            it.close();
        });
    }

    open(popup) {
        let it = this;
        
        $(`.popup_${popup}`).show().addClass('popup_opened');
        it.overlay.slideDown();
    }

    close() {
        let it = this;

        it.overlay.slideUp();

        setTimeout(() => {
            $(`.popup.popup_opened`).hide();
        }, 500);
    }
}
const overlay = new Overlay();