class Range {
    constructor () {
        let it = this;

        it.ranges = $('.el-range__squares');

        it.init();
    }

    init () {
        let it = this;

        it.ranges.each(function () {
            let range = $(this);
            let multiple = range.hasClass('el-range_single') ? true : false;
            let points = range.children('.el-range__squares-item');

            if ( multiple ) {
                let active = range.children('.el-range__squares-item.active');
                let input = range.children('input.max');

                input.val( active.attr('data-range') );

                points.on('click', function () {
                    let point = $(this);
                    let value = point.attr('data-range');

                    input.val(value);

                    points.removeClass('active');
                    point.addClass('active');
                });
            } else {
                points.each(function () {
                    let point = $(this);
                    let checked = point.hasClass('active');
                    let input = point.children('input');

                    if ( checked ) {
                        input.val(1)
                    } else {
                        input.val(0);
                    }

                    point.on('click', function () {
                        if ( checked ) {
                            input.val(0);
                            point.removeClass('active');
                        } else {
                            input.val(1);
                            point.addClass('active');
                        }
                        checked = !checked;
                    });
                });
            }
        });
    }
}
const range = new Range();