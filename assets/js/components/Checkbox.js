class Checkbox {
    constructor () {
        let it = this;

        it.checkboxes = $('.btn-checkboxes');
    }


    /**
     * Init Checkbox
     */

    init () {
        let it = this;

        it.checkboxes.each(function () {
            let checkbox = $(this);
            let btns = checkbox.find('.btn');
            let active = checkbox.find('.btn.active');
            let input = checkbox.find('input');
            input.val(active.attr('data-cb'));

            btns.on('click', function () {
                let btn = $(this);
                let value = btn.attr('data-cb');

                if ( !btn.hasClass('active') ) {
                    btns.removeClass('active');
                    btn.addClass('active');
                    input.val(value);
                }
            });
        });
    }
}
const checkbox = new Checkbox();