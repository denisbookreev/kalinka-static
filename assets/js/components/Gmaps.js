class GMaps {
    constructor () {
        let it = this;

        it.map = $('.map');
        it.openmapLink = $('.open-search-map');
        it.switchers = $('.map__switcher');
        it.tabs = $('.map__tab');
        it.regionsBtn = $('.map__button_regions');
        it.regionsClose = $('.map__regions-close');
        it.regionsList = $('.map__regions');
        it.mapBack = $('.map__back');
        
        it.switchers.on('click', function () {
            let link = $(this);
            let tab = link.attr('data-tab');

            if ( !link.hasClass('active') ) {
                it.tabs.removeClass('active');
                it.switchers.removeClass('active');

                $(`#${tab}`).addClass('active');
                link.addClass('active');
            }
        });

        it.regionsBtn.on('click', function () {
            it.regionsList.toggleClass('opened');
        });
        it.regionsClose.on('click', function () {
            it.regionsList.toggleClass('opened');
        });

        it.openmapLink.on('click', function () {
            it.map.addClass('opened');

            $('body').animate({
                scrollTop: 0
            }, 500);
        });

        it.mapBack.on('click', function () {
            it.map.removeClass('opened');
        });
    }
    
}
const gmaps = new GMaps();