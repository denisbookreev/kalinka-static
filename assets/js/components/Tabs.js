class Tabs {
    constructor () {
        let it = this;

        it.$switchers = $('.ui-tabs__switch');
        it.$insets = $('.ui-tabs__inset');

        if ( $('*').is('.ui-tabs__switch') ) {
            it.init();
        }

        it.$switchers.on('click', function () {
            let link = $(this);
            let dataTab = link.attr('data-tab');
            let inset = $(`.ui-tabs__inset[data-tabname=${dataTab}]`);

            it.$switchers.removeClass('active');
            link.addClass('active');
            it.$insets.hide();
            inset.fadeIn(500);
        });
    }

    init () {
        let it = this;
        let dataTab = $('.ui-tabs__switch.active').attr('data-tab');

        $(`.ui-tabs__inset[data-tabname=${dataTab}]`).show();
    }
}
const tabs = new Tabs();