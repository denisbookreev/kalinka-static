class Droplist {
    constructor () {
        let it = this;

        it.droplists = $('.el-droplist');
        it.$openlink = $('.open-droplist');

        it.openMiniDrop = $('.search-top-bar__droplist > span');


        it.openMiniDrop.each(function () {
            let link = $(this);
            let parent = link.closest('.search-top-bar__droplist');
            let list = link.parent().find('.search-top-bar__droplist-list');

            parent.width( parent.width() + 5 );

            link.on('click', function () {
                list.fadeToggle(300);
            });

            if ( parent.hasClass('select') ) {
                let li = list.find('a');
                let span = parent.children('span');
                let input = parent.children('input');

                li.on('click', function () {
                    list.fadeOut(300);
                    input.val( $(this).attr('data-input') );
                    span.text( $(this).text() );
                });
            }
        });
    }


    /**
     * Init Droplist
     */
    
    init () {
        let it = this;

        it.droplists.each(function () {
            let droplist = $(this);
            let span = droplist.find('span');
            let droplistName = droplist.attr('data-droplist');
            let link = droplist.find('.el-droplist__link');
            let select = droplist.find('select');
            let options = select.children('option');
            let list = $(`.el-droplist__list[data-list=${droplistName}]`);

            if ( !droplist.hasClass('loaded') ) {
                let optionsArr = [];
                let ul = $('<ul/>', {
                    'class': 'el-droplist__ul'
                }).appendTo(list);

                options.each(function () {
                    let option = $(this);
                    let value = option.val();
                    let text = option.text();

                    optionsArr.push({
                        value: value,
                        text: text
                    });
                });

                for (let i = optionsArr.length; i > 0; i--) {
                    ul.prepend(`<li class="el-droplist__li" data-li="${optionsArr[i-1].value}"><a>${optionsArr[i-1].text}</a></li>`);
                }
            }

            let ul = list.find('.el-droplist__ul');

            link.on('click', function () {
                if ( !link.hasClass('opened') ) {
                    list.fadeIn(300);
                    link.addClass('opened');
                } else {
                    list.fadeOut(300);
                    link.removeClass('opened');
                }
            });

            ul.children('li').on('click', function () {
                let li = $(this);
                let liValue = li.attr('data-li');
                let liText = li.text();

                span.text(liText);
                select.find('option:selected').attr('selected', false);
                select.find(`option[value=${liValue}]`).attr('selected', 'selected');
                
                list.fadeOut(300);
                link.removeClass('opened');
            });
        });
    }

    hide (droplist) {

        if (droplist) {
            let droplistName = droplist.attr('data-droplist');
            let link = droplist.find('.el-droplist__link');
            let list = $(`.el-droplist__list[data-list=${droplistName}]`);

            list.fadeOut(300);
            link.removeClass('opened');
        } else {
            let link = $('.el-droplist__link.opened');
            let droplistName = link.closest($('.el-droplist')).attr('data-droplist');
            let list = $(`.el-droplist__list[data-list=${droplistName}]`);

            list.fadeOut(300);
            link.removeClass('opened');
        }
    }
}
const droplist = new Droplist();