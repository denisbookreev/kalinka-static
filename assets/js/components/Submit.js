class Submit {
    constructor () {
        let it = this;

        // Variables
        it.forms = $('form');

        // Similar objects
        if ( $('*').is('#similar-objects') ) {
            let wrap = $('#similar-objects');
            let url = wrap.attr('data-url');

            it.submit('similar-objects', url);
        }

        // Submit
        it.forms.submit(function (e) {
            it.form = $(this);
            let form = it.form.attr('data-form');
            let action = it.form.attr('action');
            let data = it.form.serialize();

            if ( form === undefined ) {
                console.error('Неизвестная форма');
            } else {

                it.submit(form, action, data);
                e.preventDefault();
            }
        });
    }


    /**
     * Submit
     */
    submit (form, action, formData) {
        let it = this;
        let answer, error, email, phone, name, $form;

        if (env === 'dev' && form !== 'similar-objects') {
            action = '/app_dev.php' + action;
        }

        if (formData) {
            email = it.form.find('input[type=email]');
            phone = it.form.find('input[type=tel]');
        }

        switch (form) {
            case 'subscribe':
                answer = $('.feedback-block__answer');

                if ( assay.email(email.val()) ) {
                    // Request
                    $.ajax({
                        url: action,
                        data: formData,
                        type: 'POST',

                        // success
                        success: function (data) {
                            console.info('Данные: ', data);

                            if (data.success) {
                                it.form.hide();
                                answer.fadeIn(300);
                            } else {
                                $('.feedback-block__error').text(data.errors[0]).hide().fadeIn(300);
                            }
                        },
                        // error
                        error: function (error) {
                            console.error('Ошибка: ', error);

                            $form.effect('shake', {
                                times: 2
                            });
                        }
                    });
                } else {
                    it.form.effect('shake', {
                        times: 2
                    });
                }
                break;

            case 'main-search':
                // Request

                $(`.home-search`).find('.home-search__input').addClass('loading');

                formData = formData + '&limit=15';

                $.ajax({
                    url: action,
                    data: formData,
                    type: 'GET',

                    // success
                    success: function (data) {
                        console.info('Данные:', data);

                        let objects = data.objects.results !== undefined ? data.objects.results : false;
                        let districts = data.districts !== undefined ? data.districts : false;
                        let pages = data.pages !== undefined ? data.pages : false;
                        let offset = data.objects.total_pages;
                        let results = $('.home-search__input-results');

                        // remove preloader
                        $(`.home-search`).find('.home-search__input').removeClass('loading');

                        // clear html
                        results.html('').attr('data-offset', offset);

                        // District
                        if (districts > 0) {
                            // push district
                            results.append(`
                                <div class="el-live-result el-live-result_district">
                                    <div class="el-live-result__name">${districts[0].name}</div>
                                    <a href="/${districts[0].url}" class="el-live-result__link"></a>
                                </div>
                            `);
                        }

                        // Objects
                        if (objects.length > 0) {

                            // added count of results
                            results.append(`<p class="el-live-result__count">Найдено: ${objects.length} результатов</p>`);

                            // push results
                            for ( let i = objects.length; i > 0; i-- ) {
                                let price;

                                if ( objects[i-1].prices !== undefined ) {
                                    if ( objects[i-1].prices.sale !== undefined ) {
                                        price = objects[i-1].prices.sale.RUB + ' ₽';
                                    } else {
                                        price = 'Нет цены';
                                    }
                                } else {
                                    price = 'Нет цены';
                                }

                                if ( env === 'dev' ) {
                                    objects[i-1].url = '/app_dev.php/' + objects[i-1].url;
                                }

                                results.append(`
                                    <div class="el-live-result">
                                        <div class="el-live-result__name">${objects[i-1].offername}</div>
                                        <div class="el-live-result__info">
                                            № ${objects[i-1].lot} / ${objects[i-1].address}
                                        </div>
                                        <div class="el-live-result__bottom">
                                            <span>${price}</span>
                                        </div>
                                        <a href="/${objects[i-1].url}" class="el-live-result__link"></a>
                                    </div>
                                `);
                            }
                        } else {
                            results.append(`
                                <div class="el-live-result el-live-result_district">
                                    <div class="el-live-result__name">Нет результатов поиска</div>
                                </div>
                            `);
                        }
                        results.fadeIn(300);
                    },
                    // error
                    error: function (error) {
                        console.error('Ошибка:', error);
                    }
                });
                break;


            case 'search-page':
                // Request

                it.form.find('.home-search__input').addClass('loading');
                $('.search-result__items').css('opacity', '0.5');

                let limitObjects = 5;

                let sortData = $('form[data-form=sorting]').serialize();
                let rangeData = $('form[data-form=range-filter]').serialize();
                let fullData = `${formData}&${rangeData}&${sortData}&limit=${limitObjects}`;

                $.ajax({
                    url: action,
                    data: fullData,
                    type: 'GET',

                    // success
                    success: function (data) {
                        console.info('Данные:', data);

                        if (typeof data === 'string') {
                            console.log('упс, ошибочка вышла');
                            // remove preloader
                            $(`.home-search`).find('.home-search__input').removeClass('loading');
                            $('.search-result__founded').text(`Ничего не найдено`);
                            $('.search-result__item').remove();
                        } else {
                            let objects = data.objects.results !== undefined ? data.objects.results : false;
                            let districts = data.districts !== undefined ? data.districts : false;
                            let pages = data.pages !== undefined ? data.pages : false;
                            let results = $('.search-result__items');
                            let resultsCount = $('.search-result__founded');
                            let numberFounded = objects.length + '';
                            let offset = data.objects.total_pages;

                            results.attr('data-offset', offset);

                            // remove preloader
                            $(`.home-search`).find('.home-search__input').removeClass('loading');

                            // Founded
                            if ( numberFounded > 0 && numberFounded[numberFounded.length - 1] == 0 ) {
                                resultsCount.text(`Найдено ${numberFounded} объектов`);
                            } else if ( numberFounded[numberFounded.length - 1] == 1 ) {
                                resultsCount.text(`Найден ${numberFounded} объект`);
                            } else if ( numberFounded[numberFounded.length - 1] >= 2 &&
                                numberFounded[numberFounded.length - 1] <= 4 ) {
                                resultsCount.text(`Найдено ${numberFounded} объекта`);
                            } else if ( numberFounded[numberFounded.length - 1] >= 5 ) {
                                resultsCount.text(`Найдено ${numberFounded} объектов`);
                            } else {
                                resultsCount.text(`Ничего не найдено`);
                            }


                            let pushObjects = (items) => {
                                if (items.length > 0) {
                                    let limit;

                                    if (items.length >= 15) {
                                        limit = 15;
                                    } else {
                                        limit = items.length;
                                    }

                                    // push results
                                    for ( let i = 0; i < limit; i++ ) {
                                        let object = items.splice(0,1)[0];
                                        let priceBlock, roomText, bathroomsText, bedroomsText;

                                        if ( object.prices !== undefined ) {
                                            if ( object.prices.sale !== undefined ) {
                                                priceBlock = `
                                            <div class="estate-preview__cost">
                                                <span data-id="rub" class="cost active">${ object.prices.sale.RUB } ₽ </span>
                                                <span data-id="dol" class="cost">${ object.prices.sale.USD } $ </span>
                                                <span data-id="eur" class="cost">${ object.prices.sale.EUR } €</span>
                                                <div class="estate-preview__currency-btns"><a data-currency="rub" class="active">₽</a><a data-currency="dol">$</a><a data-currency="eur">€</a></div>
                                            </div>
                                        `;
                                            } else if ( object.prices.rent !== undefined ) {
                                                priceBlock = `
                                            <div class="estate-preview__cost">
                                                <span data-id="rub" class="cost active">${ object.prices.rent.RUB } ₽ </span>
                                                <span data-id="dol" class="cost">${ object.prices.rent.USD } $ </span>
                                                <span data-id="eur" class="cost">${ object.prices.rent.EUR } €</span>
                                                <div class="estate-preview__currency-btns"><a data-currency="rub" class="active">₽</a><a data-currency="dol">$</a><a data-currency="eur">€</a></div>
                                            </div>
                                        `;
                                            } else {
                                                priceBlock = `<div class="estate-preview__cost">Нет цены</div>`;
                                            }
                                        } else {
                                            priceBlock = `<div class="estate-preview__cost">Нет цены</div>`;
                                        }

                                        // rooms
                                        if (object.rooms === 1) {
                                            roomText = `
                                        <div class="estate-preview__rooms">
                                            <span class="number">${object.rooms}</span>
                                            <span>комната</span>
                                        </div>
                                    `
                                        } else if (object.rooms >= 2 && object.rooms <= 4) {
                                            roomText = `
                                        <div class="estate-preview__rooms">
                                            <span class="number">${object.rooms}</span>
                                            <span>комнаты</span>
                                        </div>
                                    `
                                        } else if (object.rooms >= 5) {
                                            roomText = `
                                        <div class="estate-preview__rooms">
                                            <span class="number">${object.rooms}</span>
                                            <span>комнат</span>
                                        </div>
                                    `
                                        } else {
                                            roomText = '';
                                        }

                                        // bathrooms
                                        if (object.bathrooms > 0) {
                                            bathroomsText = `<div class="estate-preview__bath"><span class="number">${object.bathrooms}</span></div>`;
                                        }  else {
                                            bathroomsText = '';
                                        }

                                        // bedrooms
                                        if (object.bedrooms > 0) {
                                            bedroomsText = `<div class="estate-preview__bad"><span class="number">${object.bedrooms}</span></div>`;
                                        }  else {
                                            bedroomsText = '';
                                        }

                                        if ( env === 'dev' ) {
                                            object.url = '/app_dev.php/' + object.url;
                                        }

                                        results.append(`
                                            <div class="search-result__item" data-id="${object.id}">
                                                <div class="estate-preview detail">
                                                    <div class="estate-preview__header">
                                                        
                                                        <a class="estate-preview__title" href="${object.url}">${object.text_type} - ${object.title}</a>
                                        
                                                        <div class="estate-preview__type"><span>${object.text_type}</span></div>
                                                        <div class="estate-preview__details">
                                                            <div class="estate-preview__area">
                                                                <span class="number">${object.area}</span>
                                                                <span>м²</span>
                                                            </div>
                                                            ${roomText}
                                                            ${bathroomsText}
                                                            ${bedroomsText}
                                                        </div>
                                                        
                                                        <div class="estate-preview__header-bottom">
                                                            <div class="estate-preview__address">${object.address}</div>
                                                            <div class="estate-preview__lot">Лот № ${object.lot}</div>
                                                        </div>
                                                        
                                                        <div class="estate-preview__header-right tablet">
                                                            ${priceBlock}
                                                            <div class="estate-preview__status">
                                                                <span>продажа</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="estate-preview__body">
                                                        <div class="estate-preview__slider">
                                                            <div class="estate-slider owl-carousel">
                                                                <div class="estate-slider__item"><img src=""></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="estate-preview__footer mobile">
                                                        ${priceBlock}
                                                        <div class="estate-preview__status"><span>продажа</span></div>
                                                    </div>
                                                </div>
                                                <!-- estate-preview-->
                                            </div>
                                        `);

                                        /**
                                         *  Price Chooser
                                         */
                                        $('.estate-preview__currency-btns a').on('click', function () {
                                            let link = $(this);
                                            let parent = $('.estate-preview__cost');
                                            let links = $('.estate-preview__currency-btns a');
                                            let costs = $('.estate-preview__cost span');

                                            if ( !link.hasClass('active') ) {
                                                let data = link.attr('data-currency');
                                                let allLinks = $(`.estate-preview__currency-btns a[data-currency=${data}]`);
                                                costs.removeClass('active');
                                                parent.find(`span[data-id=${data}]`).addClass('active');

                                                links.removeClass('active');
                                                allLinks.addClass('active');
                                            }
                                        });
                                    }
                                }
                            };

                            pushObjects(objects);
                            results.css('opacity', 1).attr('data-current', 1);

                            $(window).on('scroll', function () {
                                if  ( $(window).scrollTop() == $(document).height() - $(window).height() )
                                {
                                    let resOffset = results.attr('data-offset');
                                    let resCurOffset = results.attr('data-current');
                                    let ajax = false;


                                    if ( +resOffset > +resCurOffset && !ajax ) {
                                        ajax = true;
                                        results.css('opacity', 0.5);
                                        $.ajax({
                                            url: action,
                                            data: fullData + '&offset=' + +resCurOffset*limitObjects,
                                            type: 'GET',

                                            // success
                                            success: function (data) {
                                                console.info('Данные:', data);
                                                pushObjects(data.objects.results);
                                                results.css('opacity', 1).attr('data-current', +results.attr('data-current') + 1);
                                                ajax = false;
                                            }
                                        });
                                    } else {
                                        console.log('Все загружено или идет загрузка');
                                    }
                                }
                            });
                        }
                    },
                    // error
                    error: function (error) {
                        console.error('Ошибка:', error);
                        $('.search-result__founded').text(`Произошла ошибка`);
                        // remove preloader
                        $(`.home-search`).find('.home-search__input').removeClass('loading');
                    }
                });
                break;

            case 'similar-objects':
                $.ajax({
                    url: action + '&offset=0&limit=10',
                    type: 'GET',

                    // success
                    success: function (data) {
                        console.info('Данные:', data);

                        let objects = data.results;

                        for (let i = objects.length; i > 0; i--) {
                            let object = objects[i - 1];
                            let price, image;

                            if (object.prices.sale !== undefined) {
                                price = object.prices.sale.RUB + ' ₽';
                            } else {
                                price = 'Цена не указана';
                            }

                            if ( object.primary_image_thumbnail !== undefined && object.primary_image_thumbnail !== null ) {
                                image = object.primary_image_thumbnail;
                            } else {
                                image = '/mobile_assets/img/noimage.png';
                            }

                            $('.ui-horizontal__inner').append(`
                                <div class="ui-elect">
                                  <div style="background-image: url(${image})" class="ui-elect__image"></div>
                                  <div class="ui-elect__info">
                                    <a href="${object.url}" class="ui-elect__title">${object.offername}</a>
                                    <div class="ui-elect__details">
                                      <div class="ui-elect__details-area"><span class="number">${object.area} м²</span></div>
                                      <div class="ui-elect__details-rooms"><span class="number">${object.floor} комнаты</span></div>
                                      <div class="ui-elect__details-bath"><span class="number">${object.bathrooms}</span></div>
                                      <div class="ui-elect__details-bad"><span class="number">${object.bedrooms}</span></div>
                                    </div>
                                    <div class="ui-elect__price">${price}</div>
                                  </div>
                                </div>
                            `)
                        }
                    },
                    // error
                    error: function (error) {
                        console.error('Ошибка:', error);
                    }
                });
                break;

            case 'feedback':
                // Request

                name = it.form.find('input[type=text]');
                answer = it.form.parent().children('.popup__answer');

                if ( name.val() === '' || name.val() < 3 ) {
                    name.effect('shake', {
                        times: 2
                    });
                } else if ( phone.val() === '' ) {
                    phone.effect('shake', {
                        times: 2
                    });
                } else if ( email.val() === '' ) {
                    email.effect('shake', {
                        times: 2
                    });
                } else {

                    $.ajax({
                        url: action,
                        data: formData,
                        type: 'POST',

                        // success
                        success: function (data) {
                            console.info('Данные:', data);

                            answer.text('Спасибо, Ваша заявка принята. Наш сотрудник свяжется с Вами в ближайшее время.');
                            it.form.hide();
                            answer.fadeIn(300);
                        },
                        // error
                        error: function (error) {
                            console.error('Ошибка:', error);
                        }
                    });
                }
                break;

            case 'my-price':

                name = it.form.find('input[type=text]');
                email = it.form.find('input[type=email]');
                answer = it.form.parent().children('.popup__answer');
                let price = it.form.parent().children('input[type=number]');

                if ( name.val() === '' || name.val() < 3 ) {
                    name.effect('shake', {
                        times: 2
                    });
                } else if ( phone.val() === '' ) {
                    phone.effect('shake', {
                        times: 2
                    });
                } else if ( email.val() === '' ) {
                    email.effect('shake', {
                        times: 2
                    });
                } else if ( price.val() === '' ) {
                    price.effect('shake', {
                        times: 2
                    });
                } else {

                    $.ajax({
                        url: action,
                        data: formData,
                        type: 'POST',

                        // success
                        success: function (data) {
                            console.info('Данные:', data);

                            answer.text('Спасибо, Ваша заявка принята. Наш сотрудник свяжется с Вами в ближайшее время.');
                            it.form.hide();
                            answer.fadeIn(300);
                        },
                        // error
                        error: function (error) {
                            console.error('Ошибка:', error);
                        }
                    });
                }
                break;

            case 'hypothec':

                name = it.form.find('input[type=text]');
                email = it.form.find('input[type=email]');
                answer = it.form.parent().children('.popup__answer');

                if ( name.val() === '' || name.val() < 3 ) {
                    name.effect('shake', {
                        times: 2
                    });
                } else if ( phone.val() === '' ) {
                    phone.effect('shake', {
                        times: 2
                    });
                } else if ( email.val() === '' ) {
                    email.effect('shake', {
                        times: 2
                    });
                } else {

                    $.ajax({
                        url: action,
                        data: formData,
                        type: 'POST',

                        // success
                        success: function (data) {
                            console.info('Данные:', data);

                            answer.text('Спасибо, Ваша заявка принята. Наш сотрудник свяжется с Вами в ближайшее время.');
                            it.form.hide();
                            answer.fadeIn(300);
                        },
                        // error
                        error: function (error) {
                            console.error('Ошибка:', error);
                        }
                    });
                }
                break;

            case 'booking':
                // Request
                $.ajax({
                    url: action,
                    data: formData,
                    type: 'POST',

                    // success
                    success: function (data) {
                        console.info('Данные:', data);
                    },
                    // error
                    error: function (error) {
                        console.error('Ошибка:', error);
                    }
                });
                break;

            default:
                console.error('Неизвестная форма');
        }
    }
}
const submit = new Submit();